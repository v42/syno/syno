# syno

This [`syno` project](https://gitlab.com/v42/syno/syno) provides an overview of the projects 
in the [`syno` *group*](https://gitlab.com/v42/syno). Its purpose is to document the applications I'm running on my Synology&trade; NAS.

